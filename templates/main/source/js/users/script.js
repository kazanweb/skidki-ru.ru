$(document).ready(function () {

	window.globalPopup = new Popup();

	// $(".js-combox").combox({
	// 	startFn: function(li, index, combox) {

	// 		this.input = combox.getElementsByTagName("input")[0];

	// 		this.input.value = li.getAttribute("value");

	// 	},
	// 	changeFn: function(li, index, combox) {

	// 		var _this = this;

	// 		this.input.value = li.getAttribute("value");

	// 	}
	// });

	$("[type=tel]").mask("+7 (999) 999-99-99");

	$('.js-catalog-content').click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('.catalog__item').toggleClass('catalog__item-active');

		$(document).click(function (e) {
			$('.catalog__item').removeClass('catalog__item-active');
		})
	});

	$('.js-catalog__link').matchHeight();
	$('.js-catalog__pic').matchHeight();



	$("[data-fancybox]").fancybox({

	});

});